import { Component, OnInit } from '@angular/core';

import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-user-add',
  templateUrl: './user-add.component.html',
  styleUrls: ['./user-add.component.scss']
})
export class UserAddComponent implements OnInit {

  public newUserName = '';
  
  constructor(public userService: UserService) { }

  ngOnInit(): void {
  }

  userAdd() {
    if (this.newUserName > '') {
      this.userService.userAdd(this.newUserName);
      this.newUserName = '';
    }
  }

}
