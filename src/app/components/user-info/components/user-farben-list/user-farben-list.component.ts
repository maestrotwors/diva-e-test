import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { faPenNib, faTrashCan } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-user-farben-list',
  templateUrl: './user-farben-list.component.html',
  styleUrls: ['./user-farben-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class UserFarbenListComponent implements OnInit {
  @Input() colorEl:string = '';
  @Output() deleteColor = new EventEmitter();

  faPenNib = faPenNib;
  faTrashCan = faTrashCan;
  
  constructor() { }

  ngOnInit(): void {
  }

  delColor() {
    this.deleteColor.emit();
  }

  colorEdit() {
    alert('in process');
  }

}
