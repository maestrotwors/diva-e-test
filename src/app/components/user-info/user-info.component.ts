import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';

import { UserService } from '../../services/user.service';
import { faCirclePlus } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-user-info',
  templateUrl: './user-info.component.html',
  styleUrls: ['./user-info.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class UserInfoComponent implements OnInit {

  @Input() userInfo:any = {};
  @Input() userIndex:number = -1;
  
  faCirclePlus = faCirclePlus;
  
  constructor(public userService: UserService) { }

  ngOnInit(): void {
  }

  deleteColor(index: any) {
    this.userService.colorUserDelete(this.userIndex, index);
  }

  colorAdd() {
    alert('in process');
  }

}
