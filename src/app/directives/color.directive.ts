import { AfterViewInit, Directive, ElementRef, Input } from '@angular/core';

import { colors } from '../constants/colors';

@Directive({
  selector: '[appColor]'
})
export class ColorDirective implements AfterViewInit {
  @Input('color') color: string = 'white';

  constructor(private elRef: ElementRef) {

  }

  ngAfterViewInit(): void {
    const c = colors.find(el => el['name'] === this.color); 
    if (c !== undefined) {
      this.elRef.nativeElement.style.backgroundColor = c['value'];
    }
  }
}
