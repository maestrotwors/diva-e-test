import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor() {}

  public usersArray:any = [];

  userAdd(name: string) {
    this.usersArray.push({name, colors: [ "Gelb", "Blau", "Grün", "Orange", "Orange"]});
  }

  colorToUserAdd(userIndex: number, colorIndex: number) {

  }

  colorUserDelete(userIndex: number, colorIndex: number) {
    this.usersArray[userIndex]['colors'].splice(colorIndex, 1);
  }
}
